const Router = require('koa-router');
const router = Router({ prefix: '/suppliers' });
const { koaBody } = require('koa-body');
const { v4 } = require('uuid');

const { JsonDB, Config } = require('node-json-db');
const db_connect = new JsonDB(new Config(process.env.DB_NAME, true, false, '/'));
const ITEMS_BY_PAGE= process.env.ITEMS_BY_PAGE;


/**
 * @api GET /suppliers
 * Return list of suppliers
 */
router.get('/', async (ctx, next) => {  
  // Get query params
  let search = ctx.request.query.search??'';
  let page = parseInt(ctx.request.query.page??"1")

  // Get supplier from database
  let suppliers = await db_connect.getData("/suppliers");

  // Apply filters
  suppliers = suppliers.filter(row => {
    return row.name.toUpperCase().includes(search.toUpperCase())
  });

  // Total of pages
  let total_pages = Math.ceil(suppliers.length / ITEMS_BY_PAGE)

  //Slice paginator
  suppliers = suppliers.slice((page -1) * ITEMS_BY_PAGE, (page * ITEMS_BY_PAGE))

  // Return json response
  ctx.body = {
      data: suppliers,
      page: page,
      total_pages: total_pages
  };
})


/**
 * @api POST /suppliers
 * Create a new supplier
 */
router.post('/', koaBody(), async(ctx) => {  

    // Create supplier item
    supplier = {
      id: v4(),
      name: (ctx.request.body.name??"").trim(),
      social_reason: ctx.request.body.social_reason??"",
      address: ctx.request.body.address??"",
    }

    // Get current suppliers
    let suppliers = await db_connect.getData("/suppliers");
    //Check if supplier exists
    let index = suppliers.findIndex(row => row.name.toLowerCase() === supplier.name.toLowerCase());
    if(index >= 0){
      // Supplier exists, not allowed append
      ctx.response.status = 400;
      ctx.body = {
        "rs": false, "message": "Proveedor ya fue agregado"
      }
    }
    else{
      //Append to database
      suppliers.unshift(supplier);

      // Store in database
      await db_connect.push("/suppliers/", suppliers);

      // Return response
      ctx.response.status = 201;
      ctx.body = {
        "rs": true, "message": "Proveedor creado"
      }
    }
  
})


/**
 * @api PUT /suppliers/{id_supplier}
 * Edit an existing supplier
 */
router.put('/:id', koaBody(), async(ctx, next) => {
  // Get parameters
  let new_name = (ctx.request.body.name??"").trim();
  // Get current suppliers
  let supplier = await db_connect.getIndex("/suppliers", ctx.params.id);
  if(supplier >= 0){
    
    // Get current suppliers
    let suppliers = await db_connect.getData("/suppliers");
    //Check if supplier exists by name
    let index = suppliers.findIndex(row => row.name.toLowerCase() === new_name.toLowerCase() && row.id != ctx.params.id);
    if(index >= 0){
      // Supplier exists, not allowed append
      ctx.response.status = 400;
      ctx.body = {
        "rs": false, "message": "Proveedor ya fue agregado"
      }
    }
    else{
      // Update supplier
      await db_connect.push("/suppliers["+supplier+"]", {
        name: new_name,
        social_reason: ctx.request.body.social_reason??"",
        address: ctx.request.body.address??""
      }, false)

      ctx.response.status = 201;
      ctx.body = {
        "rs": false, "message": "Proveedor actualizado"
      }
    }
    
  }
  else{
    ctx.response.status = 404;
    ctx.body = {
      "rs": false, "message": "Proveedor no existe"
    }
  }
});

/**
 * @api DELETE /suppliers/{id_supplier}
 * Create a new supplier
 */
router.del('/:id', async(ctx, next) => {
  
    // Get current suppliers
    let supplier = await db_connect.getIndex("/suppliers", ctx.params.id);
    if(supplier >= 0){
      await db_connect.delete("/suppliers["+supplier+"]")
      // Return response
      ctx.response.status = 202;
      ctx.body = {
        "rs": true, "message": "Proveedor eliminado"
      }
    }
    else{
      ctx.response.status = 404;
      ctx.body = {
        "rs": false, "message": "Proveedor no existe"
      }
    }
});


module.exports = router;