require('dotenv').config();

const { faker } = require('@faker-js/faker');
const { JsonDB, Config } = require('node-json-db');

const db_connect = new JsonDB(new Config(process.env.DB_NAME, true, false, '/'));

let suppliers = [];

const createSupplier = () => {
  return {
    id: faker.datatype.uuid(),
    name: `${faker.company.name()}`,
    social_reason:  `${faker.name.firstName()} ${faker.name.firstName()}`,
    address: `${faker.address.streetAddress()}, ${faker.address.zipCode()} ${faker.address.city()} ${faker.address.state()} `,
    created_at: faker.date.past().toString()
  };
}

Array.from({ length: 100 }).forEach(() => {
  suppliers.push(createSupplier());
});

db_connect.push("/suppliers/", suppliers);

console.log("Seeders created!")