# Getting Started with GAPSI Test Backends

# 1)  Install dependencies (Node v16.15.1)
npm install

# 2) Copy .env.example and rename to .env

# 3) Create new seeders for database (optional) 
node start/fakers.js

# 4) Run backend server
node start